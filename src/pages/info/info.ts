import { Component } from '@angular/core';
import {
  AlertController, IonicPage, LoadingController, ModalController, NavController, NavParams,
  ViewController
} from 'ionic-angular';
import { Camera} from '@ionic-native/camera';
import firebase from 'firebase';
import {AngularFireDatabase} from "angularfire2/database";
import {AutocompletePage} from "../autocomplete/autocomplete";

declare let require;
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare let google;

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  private br : any = [];
  private admin : any = [];
  private beerId : string = '';
  public myPhotosRef: any;
  public myPhoto: any;
  private loading : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl : ViewController,
              private afDB : AngularFireDatabase, private alertCtrl : AlertController
              ,private loadingCtrl : LoadingController, private modalCtrl : ModalController,
              private camera : Camera) {
    this.myPhotosRef = firebase.storage().ref('/Logos/');
    this.beerId = this.navParams.get('beerId');
    // this.restPos = this.navParams.get('restPos');
    this.afDB.object('/admin').valueChanges().subscribe(data => {
      this.admin = data;
    });
    if (this.beerId) {
      this.afDB.object('/beers/' + this.beerId).valueChanges().subscribe(data => {
        this.br = data;
      });
    } else {
      this.br.promo = 1;
      this.br.n = 0;
      this.br.score_price = 0;
      this.br.score_rate = 0;

    }
  }

  ionViewDidLoad() {
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
  convertToNumber(event):number {  return +event; }

  saveProfile() {
    if (!this.br.address){

      let alert = this.alertCtrl.create({
        title: 'Address',
        subTitle: 'Please add address',
        buttons: ['Confirm']
      });
      alert.present();
    } else if (!this.br.name) {
      let alert = this.alertCtrl.create({
        title: 'Name',
        subTitle: 'Please add name',
        buttons: ['Confirm']
      });
      alert.present();
    } else if (!this.br.description) {
      let alert = this.alertCtrl.create({
        title: 'Description',
        subTitle: 'Please write description',
        buttons: ['Confirm']
      });
      alert.present();
    } else if (!this.br.logoUrl) {
      let alert = this.alertCtrl.create({
        title: 'Logo',
        subTitle: 'Please add logo',
        buttons: ['Confirm']
      });
      alert.present();
    } else if (!this.br.beerN1 || !this.br.beerM1 || !this.br.beerC1) {
      let alert = this.alertCtrl.create({
        title: 'Beer',
        subTitle: 'Please add at least one beer cost',
        buttons: ['Confirm']
      });
      alert.present();
    }
    else {
      this.br.score_price = this.br.beerC1/this.br.beerM1;
      if (this.br.beerM2){
        this.br.score_price = ((this.br.beerC1/this.br.beerM1)+(this.br.beerC2/this.br.beerM2))/2;
      }
      if (!this.br.beerId) {
        this.br.beerId = this.afDB.createPushId();
        this.br.isPublish = this.admin.isAgree;
        this.br.isUpdate = this.admin.isAgree;
        this.afDB.list('beers').set(this.br.beerId,this.br);
      } else {
        if (!this.br.isUpdate){
          let alert = this.alertCtrl.create({
            title: 'Update ALert',
            subTitle: 'You can not update this information',
            buttons: ['Confirm']
          });
          alert.present();
        } else {
          this.afDB.list('beers').set(this.br.beerId,this.br);
        }
      }

      this.viewCtrl.dismiss();
    }
  }
  uploadLogo() {

    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 100,
      encodingType: this.camera.EncodingType.PNG,
    }).then(imageData => {
      this.loading = this.loadingCtrl.create({content : "loading Logo..."});
      this.loading.present();
      this.myPhoto = imageData;
      this.loading.dismiss();
      this.uploadPhoto();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }
  private uploadPhoto(): void {
    this.myPhotosRef.child(this.generateUUID()).child('myPhoto.png')
      .putString(this.myPhoto, 'base64', { contentType: 'image/png' })
      .then((savedPicture) => {
        this.br.logoUrl = savedPicture.downloadURL;
      });
  }

  private generateUUID(): any {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
      let r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }

  showAddressModal () {
    let modal = this.modalCtrl.create('AutocompletePage');
    let me = this;
    modal.onDidDismiss(data => {
      me.br.address = data;
      me.geoCode(me.br.address);

    });
    modal.present();
  }
  //convert Address string to lat and long
  geoCode(address:any) {
    let address_array = address.split(',');
    let country = address_array[address_array.length-1].split(' ')[1];
    let getCurrencyAbbreviation = require('country-currency-map').getCurrencyAbbreviation;
    this.br.currency = getCurrencyAbbreviation(country);
    let geocoder = new google.maps.Geocoder();
    let me = this;
    geocoder.geocode({address : address}, function( results, status){
      if (status == google.maps.GeocoderStatus.OK){
        console.log(results[0].geometry.location.lat()+":"+results[0].geometry.location.lng());
        me.br.lat = results[0].geometry.location.lat();
        me.br.long = results[0].geometry.location.lng();
      }
    })
  }
}
