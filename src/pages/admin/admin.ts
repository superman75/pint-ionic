import {Component, ViewChild} from '@angular/core';
import {IonicPage, Navbar, NavController, NavParams} from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";

/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {
  @ViewChild(Navbar) navBar: Navbar;
  beers: any;
  admin : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private afDB :AngularFireDatabase) {
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent)=>{
      this.afDB.object('/admin').set(this.admin);
      this.navCtrl.pop();
    };
    this.afDB.object('/admin').valueChanges().subscribe(data => {
      this.admin = data;
    });
    this.beers = this.afDB.list('/beers', ref => ref.orderByChild('name')).valueChanges();
  }
  convertToNumber(event):number {  return +event; }
  changePromo(beerId : string, promo : number){
    this.afDB.object('/beers/'+beerId+'/promo').set(+promo);
  }

  setIsPublish(beerId: string , isPublish: boolean) {
    console.log(isPublish);
    this.afDB.object('/beers/'+beerId+'/isPublish').set(isPublish);
  }

  setIsUpdate(beerId: String, isUpdate: boolean) {
    this.afDB.object('/beers/'+beerId+'/isUpdate').set(isUpdate);
  }
}
