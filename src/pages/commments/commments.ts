import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import firebase from 'firebase';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the CommmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commments',
  templateUrl: 'commments.html',
})
export class CommmentsPage {

  private beerId : string;
  private rate : number;
  private score_rate : number=0;
  private rate_num : number;
  private username : string;
  private comment : string;
  private deviceId : string;

  public itemRef: firebase.database.Reference;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl : ViewController,
              private afDB : AngularFireDatabase, private alertCtrl : AlertController, private device : Device) {
    this.beerId = this.navParams.get('beerId');
    this.rate = this.navParams.get('rate');
    this.deviceId = this.device.uuid;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommmentsPage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }

  saveComments() {

    let flag = true;
    this.afDB.object('/beers/'+this.beerId+'/comments/'+this.deviceId)
      .valueChanges().subscribe(data =>{
        if (!data && flag){
          this.afDB.object('/beers/'+this.beerId+'/comments/'+this.deviceId).set({'comment': this.comment,'rate' : this.rate,'name' : this.username});
          setTimeout(()=>{
            this.rate_num = 0;this.score_rate = 0;
            firebase.database().ref('/beers/'+this.beerId+'/comments')
              .on('value', itemSnapshot =>{

                itemSnapshot.forEach(itemSnap =>{
                  this.score_rate += itemSnap.val()['rate'];
                  this.rate_num++;
                  return false;
                });
              });
            this.score_rate = this.score_rate/this.rate_num +Math.pow((1+1/this.rate_num ),this.rate_num );
            firebase.database().ref('beers/'+this.beerId)
              .update({score_rate : this.score_rate, n : this.rate_num});
          },2000);
          flag = false;
        } else if(flag) {
          let alert = this.alertCtrl.create({
            title: 'Attention!',
            subTitle: 'You have already left the comments on this restaurant.',
            buttons: ['Confirm']
          });
          alert.present();
        }
    });
    this.viewCtrl.dismiss();
  }
}
