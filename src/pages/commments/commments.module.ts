import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommmentsPage } from './commments';

@NgModule({
  declarations: [
    CommmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(CommmentsPage),
  ],
  exports: [
    CommmentsPage
  ]
})
export class CommmentsPageModule {}
