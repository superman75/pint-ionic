import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  Content, IonicPage, LoadingController, ModalController, NavController, NavParams, AlertController,
  Slides
} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import { SplashScreen } from '@ionic-native/splash-screen';
import {AngularFireDatabase} from "angularfire2/database";
import {AdminPage} from "../admin/admin";


declare let require;
declare let google;
/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  map: any;
  private beers: any = [];
  private _beer : any = [];


  private image_me : any = {
    url: "assets/imgs/me.png",
      size: new google.maps.Size(50, 60),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(25, 60),
    scaledSize: new google.maps.Size(50, 60)
  };
  private image : any = {
    url: "assets/imgs/beer-icon-black.png",
    size: new google.maps.Size(25, 30),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(12.5, 30),
    scaledSize: new google.maps.Size(25, 30)
  };
  private image_golden : any = {
    url: "assets/imgs/beer-icon-golden.png",
    size: new google.maps.Size(25, 30),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(12.5, 30),
    scaledSize: new google.maps.Size(25, 30)
  };
  private admin : any = [];
  private rates : any;
  private restPos : any;
  private rate : number = 0;
  private toolId : number = 0;
  private tabId : number = -1;
  private loading : any;
  private markers : any = [];
  private cur_markers : any = [];

  private beerId : string;
  private currentIndex : number = -1;
  private marker_me : any;

  // current position variables
  private cur_position : any;
  private cur_latlng : any;
  private cur_placeId : any;
  private des_placeId : any;

  // declare the direction variables
  private directionsService = new google.maps.DirectionsService;
  private directionsDisplay = new google.maps.DirectionsRenderer;

  //screen size variables
  private NELat : number;
  private NELng : number;
  private SWLat : number;
  private SWLng : number;


  constructor(public navCtrl: NavController, public navParams: NavParams,public geolocation : Geolocation, private modalCtrl: ModalController,
              private loadingCtrl : LoadingController, private splashScreen : SplashScreen, private afDB : AngularFireDatabase,
              private alertCtrl :  AlertController) {
    this.splashScreen.hide();
    this.loading = this.loadingCtrl.create({content : "Loading map..."});
    this.loading.present();
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;

  }

  ionViewDidLoad() {
    this.initMap();
    this.geolocation.watchPosition()
      .filter((p) => p.coords !== undefined)
      .subscribe(position=>{
        this.hideMyPosition();

        this.cur_latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        // alert(position.coords.latitude);
        if ( this.marker_me != null){
          this.marker_me.setPosition(this.cur_latlng);
        } else {
          this.marker_me = new google.maps.Marker({
            // map: this.map,
            position: this.cur_latlng,
            icon : this.image_me,
          });
        }

        this.cur_markers.push(this.marker_me);
        this.showMyPosition();
        let pos = {lat : position.coords.latitude ,lng : position.coords.longitude};
        let geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
              this.cur_placeId =  results[1].place_id;
              console.log('placeId_des : ' + this.cur_placeId);
            } else {
              console.log('No results found');
            }
          } else {
            console.log('Geocoder failed due to: ' + status);
          }
       });
    });
  }
  initMap() {
    this.geolocation.getCurrentPosition().then((cur_position) => {

      this.cur_latlng = new google.maps.LatLng(cur_position.coords.latitude, cur_position.coords.longitude);
      let latLng = new google.maps.LatLng(57.8127004, 14.2106225);
      let mapOptions = {
        center: this.cur_latlng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // disableDefaultUI: true
      };
      let pos = {lat : cur_position.coords.latitude ,lng : cur_position.coords.longitude};
      let self = this;
      let geocoder = new google.maps.Geocoder();
      geocoder.geocode({'location': pos}, function(results, status) {
        if (status === 'OK') {
          if (results[1]) {
            self.cur_placeId =  results[1].place_id;
            console.log('placeId_des : ' + self.cur_placeId);
          } else {
            console.log('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      // add control elements to map
      let centerControlDiv = document.createElement('div');
      let centerControl = new this.CenterControl(centerControlDiv, this.map,this);
      let centerControl1 = new this.CenterControl1(centerControlDiv, this.map,this);
      this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(centerControlDiv);

      // get the current position data


      // Add markers
      this.afDB.list('/beers').valueChanges()
        .subscribe(snapShots=> {


          this.beers = [];
          let i = 0;
          snapShots.forEach(snapShot=> {
            if (snapShot['isPublish']){
              this.beers.push(snapShot);

              if (this.beers[i].score_rate){
                this.beers[i].rate = (this.beers[i].score_rate-Math.pow((1+1/this.beers[i].n),this.beers[i].n)).toFixed(1);
              } else this.beers[i].rate = (0).toFixed(1);
              let getCurrency = require('country-currency-map').getCurrency;
              this.beers[i].currencyunit = getCurrency(this.beers[i].currency).symbolFormat.slice(0,-3);
              this.beers[i].price = this.beers[i].beerC1>this.beers[i].beerC2?this.beers[i].beerC2:this.beers[i].beerC1;
              this.getRate(i);
              let pos = {lat:  parseFloat(this.beers[i]['lat']), lng: parseFloat(this.beers[i]['long'])};
              let self = i;
              this.calculateDistance(i);
              let me = this;
              geocoder.geocode({'location': pos}, function(results, status) {
                if (status === 'OK') {
                  if (results[1]) {
                    console.log('test : '+results[1].place_id);
                    // console.log(i);
                    me.beers[self].placeId =  results[1].place_id;
                  } else {
                    console.log('No results found');
                  }
                } else {
                  console.log('Geocoder failed due to: ' + status);
                }
              });
              i++;
            }
          });
          // // get the screen size data
          let self = this;
          google.maps.event.addListener(this.map, "zoom_changed", function () {
            self.deleteMakers();
            let bounds = self.map.getBounds();
            let NE = bounds.getNorthEast();
            let SW = bounds.getSouthWest();
            self.NELat = NE.lat();
            self.NELng = NE.lng();
            self.SWLat = SW.lat();
            self.SWLng = SW.lng();
            self._beer = [];
            for (let i = 0; i < self.beers.length ; i++){
              if(self.beers[i]['lat']<NE.lat() && self.beers[i]['lat']>SW.lat() && self.beers[i]['long']<NE.lng() && self.beers[i]['long']>SW.lng()){
                self._beer.push(self.beers[i]);
                // this.getRate(self._beer[i]['beerId']);

                let position = new google.maps.LatLng(self.beers[i]['lat'], self.beers[i]['long']);
                let dogwalkMarker = new google.maps.Marker({
                  position: position,
                  icon : self.image
                });
                if (self.currentIndex==i && (self.toolId>-1)) {
                  dogwalkMarker.setIcon(self.image_golden);
                }
                self.markers.push(dogwalkMarker);
                google.maps.event.addListener(dogwalkMarker,'click',()=>{
                  self.map.setCenter(position);
                  self.changeToolId(0);
                });
              }
            }
            self.showMakers();
            self.returnOriginal();
          });
          google.maps.event.addDomListener(this.map,'click', function(){
            self.returnOriginal();
          });
          google.maps.event.addListener(this.map, "drag", function () {
            self.deleteMakers();
            let bounds = self.map.getBounds();
            let NE = bounds.getNorthEast();
            let SW = bounds.getSouthWest();
            self.NELat = NE.lat();
            self.NELng = NE.lng();
            self.SWLat = SW.lat();
            self.SWLng = SW.lng();
            self._beer = [];
            for (let i = 0; i < self.beers.length ; i++){
              if(self.beers[i]['lat']<NE.lat() && self.beers[i]['lat']>SW.lat() && self.beers[i]['long']<NE.lng() && self.beers[i]['long']>SW.lng()){
                self._beer.push(self.beers[i]);
                let position = new google.maps.LatLng(self.beers[i]['lat'], self.beers[i]['long']);
                let dogwalkMarker = new google.maps.Marker({
                  position: position,
                  icon : self.image
                });
                if (self.currentIndex==i && (self.toolId>-1)) {
                  dogwalkMarker.setIcon(self.image_golden);
                }
                self.markers.push(dogwalkMarker);
                google.maps.event.addListener(dogwalkMarker,'click',()=>{
                  self.map.setCenter(position);
                  self.changeToolId(0);
                });
              }
            }
            self.showMakers();
            self.returnOriginal();
          });
        });
      // this.changeToolId(-1);
      this.loading.dismiss();

    }, (err) => {
      console.log(err);
    });


  }

  returnOriginal(){
    for(let i = 1; i < 10; i++){
      this.changeToolId(-1);
    }
  }
  sortPrice(){
    this._beer.sort((ls,rs):number=>{
      if (ls.price<rs.price) return -1;
      if (ls.price>rs.price) return 1;
      return 0;
    });
  }

  sortRate(){
    this._beer.sort((ls,rs):number=>{
      if (ls.score_rate>rs.score_rate) return -1;
      if (ls.score_rate<rs.score_rate) return 1;
      return 0;
    });

  }
  sortDistance(){
    this._beer.sort((ls,rs):number=>{
      if (ls.distance_km1<rs.distance_km1) return -1;
      if (ls.distance_km1>rs.distance_km1) return 1;
      return 0;
    });
  }
  calculateScore(latLng_c){
    for (let i = 0; i < this._beer.length;i ++){
      let latLng =  new google.maps.LatLng(this._beer[i]['lat'], this._beer[i]['long']);
      let d =  google.maps.geometry.spherical.computeDistanceBetween(latLng, latLng_c);
      // this._beer[i].score = this._beer[i].score_rate*this._beer[i].r/(this._beer[i].price*this._beer[i].p+d*this._beer[i].d)+this._beer[i].promo;
      this._beer[i].score = google.maps.geometry.spherical.computeDistanceBetween(latLng, latLng_c);
    }
    this._beer.sort((ls,rs):number=>{
      if (ls.score<rs.score) return -1;
      if (ls.score>rs.score) return 1;
      return 0;
    });
  }
  changeToolId(value : number){
    if (value === -1) {
      this.toolId = -1; this.tabId = 0;
    }
    else if (this.toolId && this.toolId === value){
      this.toolId = -1; this.tabId = 0;
    } else {
      this.toolId = value; this.tabId = 1;
    }
    if (this.toolId === -1){
      this.addMarkersToMap();
      this.content.resize();
    } else{
      if (this.toolId === 1){
        this.sortPrice();
      } else if (this.toolId === 2){
        this.sortRate()
      } else if (this.toolId === 3){
        this.sortDistance();
      }
      this.slides.slideTo(0);
      this.slideChanged();
      this.content.resize();
    }
  }
  changeTabId(value : number){
    if (this.tabId === value){
      this.tabId = 1;
    } else this.tabId = value;

    if (this.tabId<1)this.toolId = 0;
    this.content.resize();
  }



  calculateDistance(index : number){


    let latLng =  new google.maps.LatLng(this.beers[index]['lat'], this.beers[index]['long']);
    this.beers[index].distance_km1 = google.maps.geometry.spherical.computeDistanceBetween(latLng, this.cur_latlng)/1000;
    this.beers[index].distance_km = (this.beers[index].distance_km1).toFixed(1);
    console.log(this.beers[index].beerId + ' : ' + this.beers[index].distance_km);
    let self = this;
    // console.log(this.cur_placeId + " " + this.beers[index].placeId);
    let response = this.directionsService.route({
      origin: {'placeId' : self.cur_placeId},
      destination:{'placeId' : self.beers[index].placeId},
      travelMode: 'DRIVING'},function(response, status){
      if (status == 'OK'){
        self.beers[index].uber_time = response.routes[0].legs[0].duration.text;
      } else {
        console.log('Directions request failed due to ' + status);
      }
    });
    response = this.directionsService.route({
      origin: {'placeId' : self.cur_placeId},
      destination:{'placeId' : self.beers[index].placeId},
      travelMode: 'WALKING'},function(response, status){
      if (status == 'OK'){
        self.beers[index].walking_time = response.routes[0].legs[0].duration.text;
      } else {
        console.log('Directions request failed due to ' + status);
      }
    });
    response = this.directionsService.route({
      origin: {'placeId' : self.cur_placeId},
      destination:{'placeId' : self.beers[index].placeId},
      travelMode: 'TRANSIT'},function(response, status){
      if (status == 'OK'){
        self.beers[index].public_time = response.routes[0].legs[0].duration.text;
      } else {
        console.log('Directions request failed due to ' + status);
      }
    });
  }

  addMarkersToMap() {

    this.deleteMakers();
    for (let i = 0 ; i<this._beer.length;  i++) {
      let position = new google.maps.LatLng(this._beer[i]['lat'], this._beer[i]['long']);
      let dogwalkMarker = new google.maps.Marker({
        position: position,
        icon: this.image
      });
      if (this.currentIndex == i && (this.toolId > -1)) {
        dogwalkMarker.setIcon(this.image_golden);
        // this.map.setCenter(position);
      }
      this.markers.push(dogwalkMarker);
      let self = this;
      google.maps.event.addListener(dogwalkMarker, 'click', () => {
        self.map.setCenter(position);
        self.calculateScore(position);
        self.changeToolId(0);
        self.content.scrollToTop();
        self.content.resize();
      });
    }

    this.showMakers();
  }
  setMapOnAll(map) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  showMakers(){
    this.setMapOnAll(this.map);
  }
  showMyPosition(){
    for (let i = 0; i < this.cur_markers.length; i++) {
      this.cur_markers[i].setMap(this.map);
    }
  }
  hideMyPosition(){
    for (let i = 0; i < this.cur_markers.length; i++) {
      this.cur_markers[i].setMap(null);
    }
    this.cur_markers = [];
  }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  deleteMakers(){
    this.clearMarkers();
    this.markers = [];
  }

  getRate(index : number){
    let ave_rate = 0;


    this.afDB.list('/beers/'+this.beers[index]['beerId']+'/comments').valueChanges()
      .subscribe(comments =>{
        let i = 0;
        this.rates = comments;
        comments.forEach(comment=>{
          ave_rate += comment['rate'];
          i++;
        });
        if (i>0) ave_rate /= i;
        // console.log(ave_rate);
        this.beers[index].ave_rate = ave_rate.toFixed(1);

      });
  }

  CenterControl(controlDiv, map,self) {

    // Set CSS for the control border.
    let controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '5px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.margin = '5px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    let controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '13px';
    controlText.style.lineHeight = '28px';
    controlText.style.fontWeight = 'bold';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Add new';
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', function() {
      self.loading = self.loadingCtrl.create({content : "please wait..."});
      self.loading.present();
      self.createModal();
      self.loading.dismiss();
    });

  }

  CenterControl1(controlDiv, map,self) {

    // Set CSS for the control border.
    let controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '5px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.margin = '5px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    let controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '13px';
    controlText.style.lineHeight = '28px';
    controlText.style.fontWeight = 'bold';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'admin';
    controlUI.appendChild(controlText);

    controlUI.addEventListener('click', function() {
      let alert = self.alertCtrl.create({
        title: 'Admin',
        inputs: [
          {
            name: 'admin',
            value : '',
            placeholder: 'Please insert password'
          },
        ],
        buttons: [{
          text : 'confirm',
          handler  : data=>{
            if (data.admin == 'admin123'){
              self.navCtrl.push(AdminPage);
            } else {
              alert('Do not match the password');
            }
          }
        }]
      });
      alert.present();
    });

  }
  createModal(){

    let profileModal = this.modalCtrl.create('InfoPage',{'restPos' : this.restPos});
    profileModal.present();

  }
  updateProfile(beerId : string){
    this.loading = this.loadingCtrl.create({content : "please wait..."});
    this.loading.present();
    let profileModal = this.modalCtrl.create('InfoPage',{beerId : beerId});
    profileModal.present();
    this.loading.dismiss();
  }

  openMap(beerId : string) {


    this.directionsDisplay.setMap(this.map);
    let self = this;
    let geocoder = new google.maps.Geocoder;
    this.afDB.object('/beers/'+beerId).valueChanges()
      .subscribe(beer=>{
        let placeId_des;
        let position = {lat: beer['lat'], lng: beer['long']};
        console.log(beer['lat'] + ' : ' + beer['long']);
        geocoder.geocode({'location': position}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
              console.log(results[1].place_id+'1');
              placeId_des =  results[1].place_id;

            } else {
              alert('No results found');
            }
          } else {
            alert('Geocoder failed due to: ' + status);
          }
        });
        // let cur_latLng;
        // self.geolocation.getCurrentPosition().then((cur_position) => {
        //   cur_latLng = {lat : cur_position.coords.latitude, lng : cur_position.coords.longitude};
        //   console.log(cur_position.coords.latitude + ' : ' + cur_position.coords.longitude);
        // });
        // geocoder.geocode({'location': cur_latLng}, function(results, status) {
        //   if (status === 'OK') {
        //     if (results[1]) {
        //       console.log(results[1].place_id+'2');
        console.log(self.cur_placeId);
        setTimeout(()=>{
          self.directionsService.route({
            origin: {'placeId' : self.cur_placeId},
            destination:{'placeId' : placeId_des},
            travelMode: 'DRIVING'
          }, (response, status) => {
            if (status === 'OK') {
              // console.log(response.routes[0].legs[0].duration.text);
              self.directionsDisplay.setDirections(response);
            } else {
              alert('Directions request failed due to ' + status);
            }
          });
        },1000);

        //     } else {
        //       alert('No results found');
        //     }
        //   } else {
        //     alert('Geocoder failed due to: ' + status);
        //   }
        // });


      })
  }

  getPlaceId(lagLng) : any{
    let geocoder = new google.maps.Geocoder;
    geocoder.geocode({'location': lagLng}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // console.log(results[1].place_id);
          return results[1].place_id;

        } else {
          alert('No results found');
        }
      } else {
        alert('Geocoder failed due to: ' + status);
      }
    });
  }
  onModelChange(beerId: string) {
    this.loading = this.loadingCtrl.create({content : "please wait..."});
    this.loading.present();
    let profileModal = this.modalCtrl.create('CommmentsPage',{rate :this.rate, beerId :beerId});
    profileModal. present();
    this.loading.dismiss();
  }

  slideChanged() {
    this.currentIndex = this.slides.getActiveIndex();
    if (this.currentIndex === this.slides.length()){
      this.currentIndex--;
    } else {
      this.getRates(this._beer[this.currentIndex]['beerId']);
      this.addMarkersToMap();
    }
  }
  getRates(beerId : string){
    this.afDB.list('/beers/'+beerId+'/comments').valueChanges()
      .subscribe(comments =>{
        this.rates = comments;

      });
  }


}
