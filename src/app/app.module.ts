import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {MainPage} from "../pages/main/main";
import { AngularFireDatabaseModule} from "angularfire2/database";
import { AngularFireModule} from "angularfire2";
import { Geolocation } from '@ionic-native/geolocation';

import { Ionic2RatingModule } from 'ionic2-rating';
import firebase from 'firebase';
import {AdminPage} from "../pages/admin/admin";
import { Device } from '@ionic-native/device';
import {Camera} from "@ionic-native/camera";
let angularConfig = {
    apiKey: "AIzaSyA3xq2p1DXFbeiCnGsZ8j6Vmp0N3Ce4Yvw",
    authDomain: "pintpoint-7449e.firebaseapp.com",
    databaseURL: "https://pintpoint-7449e.firebaseio.com",
    projectId: "pintpoint-7449e",
    storageBucket: "pintpoint-7449e.appspot.com",
    messagingSenderId: "642561494446"
};

firebase.initializeApp(angularConfig);
@NgModule({
  declarations: [
    MyApp,
    MainPage,
    AdminPage
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(angularConfig),
    IonicModule.forRoot(MyApp,{scrollAssist: false, autoFocusAssist: false}),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    AdminPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    Device,
    Camera
  ]
})
export class AppModule {}
